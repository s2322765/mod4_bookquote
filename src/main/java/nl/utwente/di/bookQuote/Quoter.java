package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    public double getBookPrice(String isbn){
        HashMap<String, Double> BookPrice = new HashMap<String, Double>();
        BookPrice.put("1", 10.0);
        BookPrice.put("2", 45.0);
        BookPrice.put("3", 20.0);
        BookPrice.put("4", 35.0);
        BookPrice.put("5", 50.0);
        if (BookPrice.containsKey(isbn)){
            return BookPrice.get(isbn);
        }else{
            return 0;
        }
        }
    }
